# smartreef-terraform-modules

This project contains Terraform modules that can be used to standardise your infrastructure-as-code configuration and deployments. It should be used as starting point only and does not have an exhausted set of modules. However, modules will be added over time.

## File structure

```/providers``` configuration the various cloud providers
```/providers/modules``` configuration for modules for a provider
```/providers/<region>``` configuration for resources in provider's region
```/providers/<region>/state``` used for the git state store
```/providers/<region>/state/<env>``` state for the specific environment

## Providers

### Files

#### Resources

Resource files should be named with the format `resources.<group>.<service>.<resource>.<name>.tf`.

##### Objects

A resource object in a file should be named with the format `<group>-<name>`.

##### Resource Names

The name property of a resource should be named with the format `<group>-<name>`.

#### Variables

The file `variables.tf` should only contain global variables. All other variables should be defined in files named with the format `variables.<group>.<service>.tf`.

## Deployment

Each providers' folder contains deployment scripts specific to each environment - `apply-<env>.sh`. These scripts export Terraform environment variables and execute the command `terraform apply`. Change directory into the script location prior to executing.

### Terraform Environment Variables

#### AWS

`TF_VAR_awsProfile` - the AWS Profile on your local machine that is used to execute commands. The profiles can be created using the AWS CLI or modifying your `~/.aws/credentials` file.

`TF_VAR_awsAccount` - the AWS Account Id that will be deployed into

`TF_VAR_awsRegion` - the AWS Region that will be deployed into

#### AWS Key Pairs

`TF_VAR_<resource type><name>PublicKeyPath` - the local file path to the public SSH key that is used to generate an AWS Key Pair. The SSH key must be created prior to executing the deployment script. Store the keys and passcode in 1Password.

### State

State is stored in the git repo if you are not using a remote state store. There is a dedicated folder for storing state `/providers/<provider>/<region>/state/<env>`

### Removing

To remove all resources created by a configuration, run the `destroy-<env>.sh` script.
