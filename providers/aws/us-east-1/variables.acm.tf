variable "acm-cert-tag-projectGroup" {
  default = "network-infrastructure"
}

variable "acm-cert-tag-project" {
  default = ""
}
