# Certificates are required to be in the `us-east-1` region

module "acm-certificate-domains-mycustomdomain-io" {
  source = "../modules/aws/acm/cert"

  name       = "mycustomdomain.io"
  domainName = "*.mycustomdomain.io"

  tags = {
    "x:projectgroup" = "${var.acm-cert-tag-projectGroup}"
    "x:project"      = "${var.acm-cert-tag-project}"
  }
}
