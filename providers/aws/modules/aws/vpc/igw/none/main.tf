# Creates a VPC that is isolated from the Internet

variable "name" {}
variable "cidrBlock" {}
variable "tag-projectGroup" {}
variable "tag-project" {}

# VPC
resource "aws_vpc" "vpc" {
  cidr_block                     = "${var.cidrBlock}"
  instance_tenancy               = "default"
  enable_dns_support             = true
  enable_dns_hostnames           = true
  enable_classiclink_dns_support = false

  tags = {
    "Name"           = "${var.name}"
    "x:projectgroup" = "${var.tag-projectGroup}"
    "x:project"      = "${var.tag-project}"
  }
}

# Expose VPC ID
output "id" {
  value       = "${aws_vpc.vpc.id}"
  description = "ID of the VPC resource created"
}
