# Creates a security group to allow traffic on MongoDB Ports

# Add the security group to the VPC
variable vpcId {}

# Tags to include on resources
variable "tags" {
  type = "map"
}

resource "aws_security_group" "mongodb" {
  name        = "MongoDB"
  description = "Allow access to MongoDB ports"
  vpc_id      = "${var.vpcId}"

  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow access to the MongoDB port"
    from_port   = 27017
    to_port     = 27017
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${var.tags}"
}

output "sgId" {
  value = "${aws_security_group.mongodb.id}"
}
