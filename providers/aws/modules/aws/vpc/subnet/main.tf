# Creates a subnet in a VPC for data resources

# Id of the VPC the subnet will be added to
variable vpcId {}

# Availability Zone for the subnet
variable subnetAvailabilityZone {}

# CIDR Block for the subnet
variable cidrBlock {}

# Assoicate the route table to the subnet
variable "routeTableId" {}

# Tags to include on resources
variable "tags" {
  type = "map"
}

# Subnets
# single digits are reserved for network devices
# First handful of IPs are reserved by AWS

# Data Subnet = 10s
resource "aws_subnet" "myNetwork" {
  vpc_id            = "${var.vpcId}"
  cidr_block        = "${var.cidrBlock}"
  availability_zone = "${var.subnetAvailabilityZone}"
  tags              = "${var.tags}"
}

# Route Table Association
resource "aws_route_table_association" "myRouteTable" {
  subnet_id      = "${aws_subnet.myNetwork.id}"
  route_table_id = "${var.routeTableId}"
}

# Expose subnet id
output "id" {
  value = "${aws_subnet.myNetwork.id}"
}
