# Creates a certificate in ACM

# Name of the resource (e.g. mycustomdomain.io)
variable "name" {}

# Domain name for which to create the certificate (e.g. *.mycustomdomain.io)
variable "domainName" {}

# Tags to associate with this certificate
variable "tags" {
  type = "map"
}

# ACM certificate
resource "aws_acm_certificate" "acm-cert" {
  domain_name       = "${var.name}"
  domain_name       = "${var.domainName}"
  validation_method = "DNS"

  tags = "${var.tags}"

  lifecycle {
    create_before_destroy = true
  }
}
