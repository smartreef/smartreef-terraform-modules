# VPC variables
variable "vpc-data-availability-zone" {
  default = "ap-southeast-2a"
}

variable "vpc-network-tag-projectGroup" {
  default = "network-infrastructure"
}

variable "vpc-network-tag-project" {
  default = ""
}

# Key Pair file references
variable "ec2KeyPairDataMongoDbPublicKeyPath" {}

variable "ec2KeyPairDataMySqlPublicKeyPath" {}
