# On this url, https://aws.amazon.com/marketplace/pp/B01M1EBY7B, click "Continue", then accept the T&Cs. 
# Look for the current version listed on the page and use it to filter for images using the AWS CLI in your terminal.
# For example, `aws ec2 describe-images --owners aws-marketplace --filters "Name=name,Values=bitnami*4.2.5-0-r01*"`
# Find the image object you want to use and use the "Name" field in the filter below

data "aws_ami" "bitnami-mongodb-4-2-1-0-r01" {
  most_recent = true

  filter {
    name   = "name"
    values = ["bitnami-mongodb-4.2.5-0-r01-linux-debian-9-x86_64-hvm-ebs-nami-dc822b28-244e-4c7f-a8eb-cb2f3b9b9a7b-ami-03f526b28a5f399dc.4"]
  }

  owners = ["679593333241"]
}
