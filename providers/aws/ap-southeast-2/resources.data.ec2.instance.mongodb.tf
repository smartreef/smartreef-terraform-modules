# EC2 instance
resource "aws_instance" "data-mongodb" {
  ami                    = "${data.aws_ami.bitnami-mongodb-4-2-1-0-r01.image_id}"
  instance_type          = "t2.micro"
  availability_zone      = "${var.vpc-data-availability-zone}"
  key_name               = "${module.ec2-keyPair-data-mongodb.keyName}"
  vpc_security_group_ids = ["${module.vpc-security-group-mongodb.sgId}", "${module.vpc-security-group-icmp.sgId}"]
  subnet_id              = "${module.vpc-subnet-data.id}"
  private_ip             = "10.0.10.10"

  # DO NOT use this as it recreates the EC2 instance if the block is changed
  #   network_interface {
  #     network_interface_id = "${aws_network_interface.ec2-eni-mongodb.id}"
  #     device_index         = 0
  #   }
  tags = {
    "Name"           = "${var.groupName-data}-mongodb"
    "x:projectgroup" = "${var.ec2-data-mongodb-tag-projectGroup}"
    "x:project"      = "${var.ec2-data-mongodb-tag-project}"
  }
}

# Elastic IP for the instance if you want to use a custom domain - e.g. data.mycustomdomain.io
resource "aws_eip" "data-mongodb-publicIp" {
  vpc = true

  tags = {
    "Name"           = "${var.groupName-data}-mongodb"
    "x:projectgroup" = "${var.ec2-data-mongodb-tag-projectGroup}"
    "x:project"      = "${var.ec2-data-mongodb-tag-project}"
  }
}

# associate the elastic ip to the instance
resource "aws_eip_association" "data-mongodb" {
  instance_id   = "${aws_instance.data-mongodb.id}"
  allocation_id = "${aws_eip.data-mongodb-publicIp.id}"
}
