#!/bin/bash
clear

# env vars
export TF_VAR_awsProfile=""
export TF_VAR_awsAccount=""
export TF_VAR_awsRegion="ap-southeast-2"

# ssh-keygen
export TF_VAR_ec2KeyPairDataMongoDbPublicKeyPath=""
export TF_VAR_ec2KeyPairDataMySqlPublicKeyPath=""

# apply with the appropriate state
terraform destroy -state=state/sandbox/terraform.tfstate